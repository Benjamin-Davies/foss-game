# FOSS Game Development

A project to create a new game for Linux and other open platforms. We're
currently working on choosing a game to make and then writing the game design
document. Bear with us.

# Join us on Matrix

https://riot.im/app/#/room/#game-dev:librem.one

# Join us on Discord

https://discord.gg/5xqVMrU